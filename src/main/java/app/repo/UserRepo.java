package app.repo;

import app.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface UserRepo extends CrudRepository<User, Long> {

    @Transactional(readOnly = true)
    User findByUsername(String username);

}
