package app.web.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class CredentialsDTO {

    private String email;
    private String password;

    public CredentialsDTO(String email, String password) {
        this.setEmail(email);
        this.setPassword(password);
    }

}
