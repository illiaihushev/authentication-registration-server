package app.service;

import app.model.User;
import app.repo.UserRepo;
import app.web.dto.CredentialsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepo userRepo;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepo userRepo, PasswordEncoder passwordEncoder) {
        this.userRepo = userRepo;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User register(CredentialsDTO credentials) {
        User user = new User(credentials.getEmail(), passwordEncoder.encode(credentials.getPassword()));
        return userRepo.save(user);
    }
}
