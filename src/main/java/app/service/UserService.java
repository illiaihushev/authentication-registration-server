package app.service;

import app.model.User;
import app.web.dto.CredentialsDTO;

public interface UserService {
    User register(CredentialsDTO credentials);
}
